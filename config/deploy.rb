# config valid for current version and patch releases of Capistrano
lock "~> 3.19.1"

set :application, "StimulusDropdown"
set :repo_url, "https://gitlab.com/sedl/stimulusfloatingdropdown.git"

set :branch, 'main'

append :linked_files, 'config/credentials/production.key'

# Default value for linked_dirs is []
append :linked_dirs, "tmp/pids", "tmp/sockets"


set :puma_service_unit_name, "puma_#{fetch(:application)}_#{fetch(:stage)}"
set :puma_systemctl_user, :user