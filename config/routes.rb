Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "floating#index"
  get 'floating/modal'
  get 'floating/stacking_context'
  get 'floating/dropdown_content'
  get 'floating/window_overflow'
  get 'floating/system_tests'
end
