class FloatingController < ApplicationController

  def index
    @debug_log = params[:console_debug_log] == "true" ? true : false
  end

  def modal

  end

  def dropdown_content
    sleep 0.5
    render layout: false
  end

  def stacking_context

  end

  def window_overflow

  end

  def system_tests

  end
end
