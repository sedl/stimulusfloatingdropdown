# Stimulus Dropdown

Dropdown with stimulus, based on floating-UI.

[Quick-and-dirty demo](https://stimulus-dropdown.sedlmair.ch/)

## Example

html like

```html
<div data-controller="csedl-dropdown" data-toggle="any-unique-id">
    Button
</div>
<div id="any-unique-id" class="hide dropdown-panel-example-class">
    ... any content
</div>
```
## What it does

- When the button is clicked, it toggles the `hide` class on the panel and places the panel using floating-ui.
- When the panel is open, the `has-open-panel` class is added to the button, otherwise it is removed.
- Adds functinality for closing all panels on clicking outside a panel.
- This all works with stacked panels too (panel in panel).

## Requirements

- The class `dropdown-panel-example-class` must be set to `position: absolute;`.
- The class `hide` must be set to `display: none;`.

## Configs
 Create a meta tag inside the header, like this:

```html
<meta content="console-debug-log=true; close-panel-selector=.close-panel-button;" name="stimulus-dropdown">
```

- `console-debug-log` default: false, is more for internal use or to check if the settings have been applied.
- `close-panel-selector` When the panel is opened, it searches within the panel element for `panel.querySelectorAll(selector)` and adds the close-panel (onclick) functionality.
- `dropdown-content-wrapper-selector` When the panel is opened and `data-src` attribute given, it replaces the wrapper within the dropdown-panel, by the result of the http-request.
- `tooltip-content-wrapper-selector` 

## Options

- If a `data-src="/any-route"` attribute is set on the panel element, when it is opened it will send a GET request to the server and, upon receiving the result, place it inside the panel element and trigger the replace-panel function a second time.
- If there is an element with ID `arrow` inside the dropdown panel, it is treated as described on [floating-ui](https://floating-ui.com/docs/arrow).

## Events

Events on the button element:

- `place-panel` places the panel, and, if present, the arrow element, by `floating-ui`.

Events the panel element:

- `close` closes the panel.
- `place-me` like place-panel on the button.

Event Triggers on the button element:

- `before-open-panel`
- `after-close-panel`

Event Triggers on the panel element:

- `before-open`

## Helpers

If the panels are rendered to a different location than the button (see z-index on [rails-app](https://gitlab.com/sedl/stimulusfloatingdropdown)), within a scrollable (e.g.) container, the button would scroll away from the panel. For such cases, add this both data-attributes to the scrollable element:

```html
<div data-controller="csedl-place-dropdown-panels" data-on="scroll" data-run-after="500" style="overflow: scroll;">
    ...
</div>
```

Now, on scrolling, it searches for all dropdown-buttons (by class-name `has-open-panel`) and triggers the `place-panel` event there.

**Options**

`data-on` Attribute:

- `scroll` triggered by `scroll` Event of the given element.
- `resize-observer` triggered by [ResizeObserver](https://developer.mozilla.org/en-US/docs/Web/API/ResizeObserver) on the given element.

`data-run-after` Attribute:

- Milliseconds as number.

This is only relevant if you have things like `css transition` enabled, so that after the above resize events are fired, subsequent events are needed. It will fire the `place-panel` after the last resize/scroll event within the given time.

**Tip** Turn `console-debug-log` on (see configs) and check how events are working.

**Explanation**

What these helpers mainly do is to find all the dropdowns by the `has-open-panel` class and fire the `place-panel` event. But within the helper, things like performance optimisation are done: it searches once and places the panels multiple times.

## Tooltip

```html
<span data-controller="csedl-tooltip" data-toggle="tooltip-123" data-delay="0.2">
    Text-with-tooltip
</span>
<div id="tooltip-123" class="hide tooltip-panel">
    <div id="arrow"></div>
    ... any content
</div>
```

makes a tooltip.

It adds the class `tooltip-is-visible` to the tooltip label while the tooltip is visible.

`data-src` attribute is working similar to dropdown

## Rails Helpers

There are examples with view-helpers, on [GitLab](https://gitlab.com/sedl/stimulusfloatingdropdown)

**License**

MIT