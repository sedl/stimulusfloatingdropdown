module DropdownHelper

  #== dropdown helper
  # @param [button_content] Proc or String for content of the button element
  # @param [panel_at_place] true (default: false) => it would render the panel into content_for(:dropdown_panels) on sticky parts (left-menu / top-bar) panels should be at place for avoiding that the panel would scroll with the content
  # @block [panel_content] Panel Content as block
  # what it does:
  # a) wraps the button_content in element like: %button{ data: { controller: 'dropdown', ... } } and renders it at place
  # b) wraps the panel_content in a corresponding element and renders it to the dropdown_panels-box (because of z-index-hierarchy)
  # c) the stimulus controller always adds the class .floating-dropdown-panel to the panel
  def dropdown(button_content, title = false, options = {}, &panel_content)

    if title.is_a?(Hash)
      options = title.dup
      title = false
    end

    panel_at_place = options.delete(:panel_at_place) || false
    placement = options.delete(:placement) || :bottom

    id = if options.key?(:id)
           raise ":id option is not allowed. Use :button_id and/or :panel_id"
         elsif options.key?(:panel_id)
           options.delete(:panel_id)
         else
           "dropdown-panel-#{SecureRandom.hex(4)}"
         end

    button_id = options.delete(:button_id)

    src = options.delete(:src)

    # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    # create the Button
    # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

    button_options = options.dup
    button_options[:class] = [options[:class], 'dropdown-button'].compact.join(' ')
    button_options = button_options.merge(data: { controller: 'csedl-dropdown', toggle: id })

    r = content_tag(:button, button_options.merge(id: button_id)) do
      if button_content.is_a?(Proc)
        capture(&button_content)
      elsif button_content.is_a?(Symbol) || button_content.is_a?(String)
        button_content
      end
    end

    # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    # create the panel
    # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

    panel_options = options.dup
    panel_options[:class] = [options[:class], 'dropdown-panel hide'].compact.join(' ')
    panel_options[:id] = id
    panel_options[:placement] = placement
    panel_options['data-src'] = src if src

    panel = content_tag(:div, panel_options) do
      r2 = content_tag :div, class: 'header' do
        r3 = content_tag :div, class: 'title' do
          title
        end
        r3 << content_tag(:span, class: 'close-panel-button') {'X'}
      end
      r2 << content_tag(:div, class: 'panel-content') do
        capture(&panel_content) if block_given?
      end
      r2 << content_tag(:div, id: 'arrow') do
        nil
      end
    end

    # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    # return the result
    # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

    if panel_at_place
      r << panel
    else
      r << content_for(:dropdown_panels) do
        panel
      end
    end

  end

end