module TooltipHelper

  def tooltip(label, options = {}, &content)

    panel_at_place = options.delete(:panel_at_place) || false
    delay = options.delete(:delay) || 0.1
    src = options.delete(:src)

    id = ['tooltip', SecureRandom.hex(4)].join('-')

    lab = if label.is_a?(Proc)
            capture &label
          else
            label
          end
    cont = capture(&content) if block_given?

    # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    # create the Label
    # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

    label_options = options.dup
    label_options[:class] = [options[:class], 'tooltip-label'].compact.join(' ')
    label_options = label_options.merge(data: { controller: 'csedl-tooltip', toggle: id, delay: delay })

    label_element = if block_given? && cont.present?
                      content_tag(:span, label_options) do
                        lab
                      end
                    else
                      lab
                    end

    # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
    # create the panel
    # xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

    panel_options = options.dup
    panel_options[:class] = [options[:class], 'tooltip-panel hide'].compact.join(' ')
    panel_options[:id] = id
    panel_options['data-src'] = src if src

    panel_element = if block_given? && cont.present?
                      content_tag(:div, panel_options) do
                        r2 = content_tag :div, id: 'arrow' do
                          ; nil;
                        end
                        r2 << content_tag(:div, class: 'tooltip-content') do
                          cont
                        end
                      end
                    end

    if !panel_element
      label_element
    elsif panel_at_place
      r = label_element
      r << panel_element
    else
      content_for(:dropdown_panels) do
        panel_element
      end
      label_element
    end

  end
end