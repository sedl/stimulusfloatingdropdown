# README

This is a quick-and-dirty example app (Rails and Vite as frontend engine) with a stimulus controller that handles a dropdown based on floating-ui.
See [Quick-and-dirty demo](https://stimulus-dropdown.sedlmair.ch/).

## Installing the drop-down in your application

Install the NPM Package: [@csedl/stimulus_dropdown](https://www.npmjs.com/package/@csedl/stimulus_dropdown)

Import it: 

`import '@csedl/stimulus_dropdown'`

Copy and paste the [dropdown helper](https://gitlab.com/sedl/stimulusfloatingdropdown/-/blob/main/app/helpers/dropdown_helper.rb) into your app and customize the code.

Provide a minimal css:

```css
.dropdown-button {
    cursor: pointer;
}

.dropdown-panel {
    border: 1px solid black;
    margin: 1rem;
    padding: 1rem;
    max-width: 10rem;
    position: absolute;
    background-color: white;

    .header {
        .close-panel-button {
            float: right;
            cursor: pointer;
        }
    }
}

.hide {
    display: none;
}
```

## z-index issues

When the dropdown-panel is rendered on the same place like the button, it may not overlap like desired, see discussion on [GitHub](https://github.com/floating-ui/floating-ui/issues/482#issuecomment-1790926611).
So this helper renders, by default, panels by `content_for :dropdown_panels`.
Insert into the layout where you can define z-indexes more at the root level:

```html
<%= yield :dropdown_panels %>
```

## Usage

Insert in a view:

```html.erb
<%= dropdown('Button') do %>
  Content
<% end %>
```

And the dropdown should work!

**What it does**

- It wraps the content for the button in an element that corresponds to the stimulus controller (NPM package).
- Wraps the second block inside an element that the NPM package treats as a panel.

**More Options**

```html
<% btn = Proc.new do %>
    <%= I18n.locale.upcase %>
    <div>..</div>
<% end %>
<%= dropdown(btn, _('Sprache auswählen'), class: 'language-select', local_panel: true, src: '/any/path') do %>
    <div>..</div>
<% end %>
```

- The button content can also be a proc, for rendering html there.
- `title` (second argument) sets a title within the panel
- `class` adds this class/-es to the button element and the panel element.
- `src` sets a `data-src` attribute to the panel, see NPM package
- `local_panel` if set to true, it renders the panel at place (not yield it to the :dropdown_panels).

## Tooltip

Example Helper for a tooltip is [here](https://gitlab.com/sedl/stimulusfloatingdropdown/-/blob/main/app/helpers/tooltip_helper.rb)
Example CSS for a tooltip is [here](https://gitlab.com/sedl/stimulusfloatingdropdown/-/blob/main/app/frontend/stylesheets/tooltip.scss)

**Licencse**

MIT