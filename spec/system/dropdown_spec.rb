require 'rails_helper'

RSpec.describe "dropdowns", type: :system do

  context 'as admin' do

    it 'open dropdown' do
      visit root_path
      expect(page).not_to have_css('#panel') # checks only for visible css
      find('#drop-btn').click
      expect(page).to have_css('#panel')
    end

    context 'close on click outside' do

      it 'open dropdown and close on click outside' do
        visit root_path
        expect(page).not_to have_css('#panel') # checks only for visible css
        find('#drop-btn').click
        expect(page).to have_css('#panel')
        find('body').click
        expect(page).not_to have_css('#panel')
      end

      it 'open dropdown and close previous on click next dropdown' do
        visit root_path
        expect(page).not_to have_css('.panel-4') # checks only for visible css
        btn4 = find('#drop-btn4')
        btn4.click
        expect(page).to have_css(".panel-4")
        expect(page).not_to have_css('.second-panel')
        find('#drop-btn2').click
        expect(page).not_to have_css('.panel-4')
        expect(page).to have_css('.second-panel')
      end

      it 'do not close parents on nested dropdowns' do
        visit root_path
        expect(page).not_to have_css('#panel-5')
        expect(page).not_to have_css('#panel-51')
        expect(page).not_to have_css('#panel-52')

        find('#drop-btn5').click
        expect(page).to have_css('#panel-5')
        expect(page).not_to have_css('#panel-51')
        expect(page).not_to have_css('#panel-52')

        find('#drop-btn51').click
        expect(page).to have_css('#panel-5')
        expect(page).to have_css('#panel-51')
        expect(page).not_to have_css('#panel-52')

        find('#drop-btn52').click
        expect(page).to have_css('#panel-5')
        expect(page).to have_css('#panel-51')
        expect(page).to have_css('#panel-52')
      end

    end

    context 'toggle class has-panel-open' do

      before(:each) do
        visit floating_system_tests_path

        expect(page).not_to have_css('#drop-btn.has-panel-open')
        expect(page).not_to have_css('#panel')

        find('#drop-btn').click

        expect(page).to have_css('#drop-btn.has-panel-open')
        expect(page).to have_css('#panel')
      end

      it 'close by second click on button' do
        find('#drop-btn').click
        expect(page).not_to have_css('#drop-btn.has-panel-open')
        expect(page).not_to have_css('#panel')
      end

      it 'close by click on close button' do
        find('#panel > .close-dropdown').click
        expect(page).not_to have_css('#panel')
        expect(page).not_to have_css('#drop-btn.has-panel-open')
      end

      it 'close by click outside' do
        find('body').click
        expect(page).not_to have_css('#panel')
        expect(page).not_to have_css('#drop-btn.has-panel-open')
      end
    end
  end

end